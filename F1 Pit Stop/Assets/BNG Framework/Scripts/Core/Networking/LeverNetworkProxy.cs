﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Photon.Pun;
using BNG;

namespace RealSim
{
    public class LeverNetworkProxy : MonoBehaviour
    {

        #region variables

        private PhotonView photonView;

        private Lever lever;

        #endregion

        private void OnEnable()
        {
            CheckForAndAssignRequiredComponets();
            lever.onLeverChange.AddListener(OnLeverChangedCall);
            lever.onLeverDown.AddListener(OnLeverDownCall);
            lever.onLeverUp.AddListener(OnLeverUpCall);
        }

        private void OnDisable()
        {
            lever.onLeverChange.RemoveListener(OnLeverChangedCall);
            lever.onLeverDown.RemoveListener(OnLeverDownCall);
            lever.onLeverUp.RemoveListener(OnLeverUpCall);
        }

        void CheckForAndAssignRequiredComponets()
        {
            if (GetComponent<PhotonView>())
            {
                photonView = GetComponent<PhotonView>();
            }
            else
            {
                Debug.LogError("No photon view attached to object: " + transform.name);
                this.enabled = false;
            }

            if (GetComponent<Lever>())
            {
                lever = GetComponent<Lever>();
            }
            else
            {
                Debug.LogError("No lever attached to object: " + transform.name);
                this.enabled = false;
            }
        }

        #region remote procedure calls

        //up down change

        public void OnLeverChangedCall(float percentage)
        {
            percentage = lever.LeverPercentage;
            if(PhotonNetwork.IsConnected)
                photonView.RPC("OnLeverChanged", RpcTarget.Others, percentage);
        }

        [PunRPC]
        public void OnLeverChanged(float percentage)
        {
            lever.onLeverChange.Invoke(percentage);
        }

        public void OnLeverDownCall()
        {
            if (PhotonNetwork.IsConnected)
                photonView.RPC("OnLeverDown", RpcTarget.Others);
        }

        [PunRPC]
        public void OnLeverDown()
        {
            lever.onLeverDown.Invoke();
        }

        public void OnLeverUpCall()
        {
            if (PhotonNetwork.IsConnected)
                photonView.RPC("OnLeverUp", RpcTarget.Others);
        }

        [PunRPC]
        public void OnLeverUp()
        {
            lever.onLeverUp.Invoke();
        }

        #endregion

    }
}
