﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using BNG;
using Photon.Pun;

namespace RealSim
{
    public class SnapZoneNetworkProxy : MonoBehaviour
    {

        #region variables

        private PhotonView photonView;

        private SnapZone snapZone;

        #endregion

        private void OnEnable()
        {
            CheckForAndAssignRequiredComponents();
            snapZone.OnSnapEvent.AddListener(OnSnappedCall);
            snapZone.OnDetachEvent.AddListener(OnDetachedCall);
        }

        private void OnDisable()
        {
            snapZone.OnSnapEvent.RemoveListener(OnSnappedCall);
            snapZone.OnDetachEvent.RemoveListener(OnDetachedCall);
        }

        void CheckForAndAssignRequiredComponents()
        {
            if (GetComponent<PhotonView>())
            {
                photonView = GetComponent<PhotonView>();
            }
            else
            {
                Debug.LogError("No photon view attached to object: " + transform.name);
                this.enabled = false;
            }

            if (GetComponent<SnapZone>())
            {
                snapZone = GetComponent<SnapZone>();
            }
            else
            {
                Debug.LogError("No snap zone attached to object: " + transform.name);
                this.enabled = false;
            }
        }

        #region Remote Procedure Calls

        public void OnSnappedCall(Grabbable grabbable)
        {
            photonView.RPC("SetHeldItem", RpcTarget.Others, snapZone.HeldItem);
            photonView.RPC("OnSnapped", RpcTarget.Others);
            Debug.Log("<color=yellow>Snapped: " + snapZone.HeldItem.name + "</color>");
        }

        [PunRPC]
        void SetHeldItem(Grabbable heldItem)
        {
            snapZone.HeldItem = heldItem;
        }

        [PunRPC]
        void OnSnapped()
        {
            snapZone.OnSnapEvent.Invoke(snapZone.HeldItem);
        }

        public void OnDetachedCall(Grabbable grabbable)
        {
            Debug.Log("<color=red>Detached: " + snapZone.HeldItem.name + "</color>");
            photonView.RPC("OnDetached", RpcTarget.Others);
        }

        [PunRPC]
        void OnDetached()
        {
            snapZone.OnDetachEvent.Invoke(snapZone.HeldItem);
        }

        #endregion
    }
}
