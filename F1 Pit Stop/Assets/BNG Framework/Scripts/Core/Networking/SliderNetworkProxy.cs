﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Photon.Pun;
using BNG;

namespace RealSim
{
    public class SliderNetworkProxy : MonoBehaviour
    {

        #region variables

        private PhotonView photonView;

        private Slider slider;

        #endregion

        private void OnEnable()
        {
            CheckForAndAssignRequiredComponets();
            slider.onSliderChange.AddListener(OnSliderChangedCall);
        }

        private void OnDisable()
        {
            slider.onSliderChange.RemoveListener(OnSliderChangedCall);
        }

        void CheckForAndAssignRequiredComponets()
        {
            if (GetComponent<PhotonView>())
            {
                photonView = GetComponent<PhotonView>();
            }
            else
            {
                Debug.LogError("No photon view attached to object: " + transform.name);
                this.enabled = false;
            }

            if (GetComponent<Slider>())
            {
                slider = GetComponent<Slider>();
            }
            else
            {
                Debug.LogError("No slider attached to object: " + transform.name);
                this.enabled = false;
            }
        }

        #region remote procedure calls

        public void OnSliderChangedCall(float percentage)
        {
            percentage = slider.SlidePercentage;
            if(PhotonNetwork.IsConnected)
                photonView.RPC("OnSliderChanged", RpcTarget.Others, percentage);
        }

        [PunRPC]
        public void OnSliderChanged(float percentage)
        {
            //slider.onSliderChangeEvent;
            slider.onSliderChange.Invoke(percentage);
        }

        #endregion

    }
}
