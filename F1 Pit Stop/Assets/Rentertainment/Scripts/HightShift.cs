﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

/// <summary>
/// Control the height and location of the player, shifting and resetting with the keyboard
/// </summary>

public class HightShift : MonoBehaviour
{

    public GameObject cameraHolder;
    public GameObject camera;
    public float y = 1f;
    float x;
    float z;

    public Vector3 trueAxis;
    public Vector3 rot;    //rotation of camera

    // Use this for initialization

    public static HightShift Instance;


    void Start()
    {
        DontDestroyOnLoad(gameObject);

        trueAxis.x = cameraHolder.transform.localPosition.x;
        trueAxis.z = cameraHolder.transform.localPosition.z;
    }

    // Update is called once per frame
    void Update()
    {
        // Up/Down controls
        if (Input.GetKey(KeyCode.DownArrow))
            transform.position -= new Vector3(0, 0.01f, 0);
        if (Input.GetKey(KeyCode.UpArrow))
            transform.position += new Vector3(0, 0.01f, 0);

        //Left/Right Controls
        if (Input.GetKey(KeyCode.A))
            transform.position -= new Vector3(0.01f, 0, 0);
        if (Input.GetKey(KeyCode.D))
            transform.position += new Vector3(0.01f, 0, 0);

        //Forward/Backward controls
        if (Input.GetKey(KeyCode.S))
            transform.position -= new Vector3(0, 0, 0.01f);
        if (Input.GetKey(KeyCode.W))
            transform.position += new Vector3(0, 0, 0.01f);

        //Rotation
        if (Input.GetKey(KeyCode.Q))
            transform.eulerAngles -= new Vector3(0, 0.5f, 0);
        if (Input.GetKey(KeyCode.E))
            transform.eulerAngles += new Vector3(0, 0.5f, 0);

        if (Input.GetKeyDown(KeyCode.Slash))    //reset height
        {
            y = -camera.transform.localPosition.y;
            trueAxis.y = y;
            cameraHolder.transform.position = trueAxis;
        }

        if (Input.GetKeyDown(KeyCode.RightControl)) //reset xyz locations
        {
            trueAxis.x = -camera.transform.localPosition.x;
            trueAxis.z = -camera.transform.localPosition.z;
            cameraHolder.transform.position = trueAxis;
        }

        if (Input.GetKeyDown(KeyCode.RightAlt)) //reset rotation
        {
            y = -camera.transform.eulerAngles.y;
            rot = new Vector3(cameraHolder.transform.eulerAngles.x, cameraHolder.transform.eulerAngles.y + y, cameraHolder.transform.eulerAngles.z);
            cameraHolder.transform.eulerAngles = rot;
        }

        if (Input.GetKeyDown(KeyCode.Escape)) //nothing to do with Height, simply allows leaving the app when Esc is hit
        {
            Application.Quit();
        }
    }
}
