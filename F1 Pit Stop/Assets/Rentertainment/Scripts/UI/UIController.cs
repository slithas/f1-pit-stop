using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// UI controller, mostly to make sure that the right text shows up
/// </summary>

public class UIController : MonoBehaviour
{
    public Text timer;
    public Text[] playerTimes = new Text[4];
    public Text highscore;
    GameplayController gc;
    public Text playerNoText;

    // Start is called before the first frame update
    void Start()
    {
        gc = GameplayController.Instance;
    }

    // Update is called once per frame
    void Update()
    {
        playerNoText.text = "Players: " + GameplayController.Instance.playerNo;

        if (!gc.startTimer)
            timer.text = "Time: 0.00";
        else if(gc.roundDone)
            timer.text = "Done!";
        else
            timer.text = "Time: " + (Mathf.Round(gc.timer * 100)) / 100;

        for (int i = 0; i < 4; i++)
        {
            if (gc.time[i] == 0)
                playerTimes[i].text = "P" + (i + 1) + ":";
            else
            {
                playerTimes[i].text = "P" + (i + 1) + ": " + Mathf.Round(gc.time[i] * 100) / 100;
                playerTimes[i].color = Color.green;
            }
        }
        highscore.text = "Best: " + Mathf.Round(gc.bestTime * 100) / 100;
    }
}
