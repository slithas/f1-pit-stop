using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Runs the Particle effect if the correct number of players is chosen
/// </summary>
public class PlayerMarkers : MonoBehaviour
{
    public int player;
    GameplayController gc;
    ParticleSystem particle;

    // Start is called before the first frame update
    void Start()
    {
        gc = GameplayController.Instance;
        particle = GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        if (player <= gc.playerNo)
        {
            if (!particle.isPlaying)
            {
                particle.Play();
            }
        }
        else
        {
            particle.Stop();
        }
    }
}
