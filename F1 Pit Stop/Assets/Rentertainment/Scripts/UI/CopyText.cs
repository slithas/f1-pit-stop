﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CopyText : MonoBehaviour
{
    Text text;
    public Text copy;
    public bool colour;

    // Use this for initialization
    void Start()
    {
        text = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        if (copy != null)
        {
            if (text.gameObject.activeInHierarchy)
                text.text = copy.text;
            else
                text.text = " ";

            if (colour)
                text.color = copy.color;
        }
    }
}
