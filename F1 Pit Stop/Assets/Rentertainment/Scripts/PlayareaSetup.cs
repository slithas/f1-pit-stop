using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayareaSetup : MonoBehaviour
{
    public GameObject drillGhost1;
    public GameObject drill;
    public GameObject drillZone1;
    bool started;
    GameplayController gc;
    int runNo;

    // Start is called before the first frame update
    void Start()
    {
        gc = GameplayController.Instance;
        drillGhost1.SetActive(false);
        drill.GetComponent<BNG.Grabbable>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (runNo != gc.runNo)
        {
            runNo = gc.runNo;
            started = false;
            drillZone1.SetActive(true);
        }

        if (gc.startTimer && !started)
        {
            started = true;
            drillGhost1.SetActive(true);
            drill.GetComponent<BNG.Grabbable>().enabled = true;
        }
    }
}
