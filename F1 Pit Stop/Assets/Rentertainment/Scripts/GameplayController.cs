using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// Script with the overarching control of the gameplay mechanics and the Networking communications
/// </summary>
public class GameplayController : MonoBehaviour
{
    public static GameplayController Instance;

    public int playerNo = 1; //number of players in the upcoming round
    public float[] time = new float[4];//players scores

    //the following are used for timer sync
    public bool countdown = false;
    public bool startTimer = false;
    public float timer;
    public float startTime = 3;
    double maxTime = 60;
    public float bestTime = 0;    //best score of this run
    public int runNo = 0;
    public bool roundDone;

    OVRScreenFade FadeScript;

    private void Awake()
    {
        if(Instance==null)
            Instance = this;
    }

    private void Start()
    {
        Invoke("GetFader",0.1f);
    }

    void GetFader() { FadeScript = OVRScreenFade.instance; }

    void Update()
    {
        //input control
        if (Input.GetKeyDown(KeyCode.Return)) { GameplayNetworker.Instance.StartSequence();}
        if (Input.GetKeyDown(KeyCode.Alpha1)) { GameplayNetworker.Instance.SetPlayerNo(1); }
        if (Input.GetKeyDown(KeyCode.Alpha2)) { GameplayNetworker.Instance.SetPlayerNo(2); }
        if (Input.GetKeyDown(KeyCode.Alpha3)) { GameplayNetworker.Instance.SetPlayerNo(3); }
        if (Input.GetKeyDown(KeyCode.Alpha4)) { GameplayNetworker.Instance.SetPlayerNo(4); }

        //Function keys
        if(Input.GetKeyDown(KeyCode.LeftShift)) timer = 60;
        if (Input.GetKeyDown(KeyCode.Backslash)) bestTime=0;

        //3 2 1 countdown till start control
            if (countdown && !startTimer)
        {
            startTime -= Time.deltaTime;
            if (startTime <= 0)
                startTimer = true;
        }
        //Timer control
        if (!startTimer) return;
        if (!roundDone) timer += Time.deltaTime;
        if (timer >= maxTime) //if this goes off it means the time limit has been reached
        {
            timer = 60;
            for (int i = 0; i < 4; i++)
            {
                if (time[i] == 0)
                    time[i] = 60;
            }
        }
        //check if everyone has finished (or reached the end of the time)
        if (!roundDone)
        {
            int finishCheck = 0;
            for (int i = 0; i < 4; i++)
            {
                if (time[i] != 0)//if a player is done(or the timer is up), the finishCheck goes up
                    finishCheck++;
            }
            if (finishCheck < playerNo)//if finishCheck does not equal or surpass playerNo, return, otherwise go into ending mode
                return;

            FindBestTime();
            roundDone = true;
            StartCoroutine(ResetScores());
            StartSequence();
            if (PhotonNetwork.IsMasterClient)
                DelayedChangeScene.Instance.DelayedChange();    //activate scenechange for animation of incoming car
        }
    }


    public void Countdown() { GameplayNetworker.Instance.Countdown(); }
    public void SetScore(int player) { GameplayNetworker.Instance.SetScore(player); }

    //This runs to find the slowest time of this run, and then check if it's faster than any others thus far
    void FindBestTime()
    {
        float bestScore = 0;
        for (int i = 0; i < 4; i++) //find the slowest score from this run
        {
            if (time[i] > bestScore)
                bestScore = time[i];
        }
        if (bestTime > bestScore || bestTime == 0) //compare if the bestScore is faster than previous runs
        {
            bestTime = bestScore;
        }
    }
    IEnumerator ResetScores() //clense all for the next run
    {
        Debug.Log("Resetting Scores");
        //if (PhotonNetwork.IsMasterClient)
          //  DelayedChangeScene.Instance.DelayedChange();//change to animation scene, then clear the data after five seconds
        runNo++;
        yield return new WaitForSeconds(5);
        for (int i = 0; i < 4; i++)
            time[i] = 0;
        countdown = false;
        startTimer = false;
        timer = 0;
        startTime = 3;
        roundDone = false;

        ///NOTE: Add element here for resetting the Playareas and maybe triggering the animations of the car going/showing
    }

    /// <summary>
    /// Teleport user during scenario
    /// </summary>
    public void TeleportPlayer(Vector3 newPosition, bool fadeOut = true)
    {        
        FadeTo(0.5f, () =>
        {
            PlayerBaseController.Instance.transform.position = new Vector3(newPosition.x, newPosition.y, newPosition.z); //originally .y had a +0.4f added

            if (fadeOut)
                FadeFrom(1);
        });
    }

    public void StartSequence()
    {
        if (runNo == 0)
        {
           GameplayNetworker.Instance.TeleportPlayer(new Vector3(-8, 0, 0));
            bestTime = 0;  //reset the top score for a new round
        }
    }

    /// <summary>
    /// Reset all players once the MasterClient has reached the third cycle of gameplay
    /// </summary>
    public void ResetSequence()
    {
        TeleportPlayer(new Vector3(0, 0, 0));
        runNo = 0;
       // this.photonView.RPC("RPCResetSequence", RpcTarget.AllBuffered);
    }

    [PunRPC]
    void RPCResetSequence()
    {
        TeleportPlayer(new Vector3(0, 0, 0));
        runNo = 0;
    }

    /// <summary>
    ///  The following is the FadeTo and FadeFrom controls
    /// </summary>
    /// 
    /// 
    /// <summary> Fade to black </summary>
    public void FadeTo(float time, UnityAction onFinishedFading)
    {
        FadeScript.fadeTime = time;
        FadeScript.FadeToBlack(onFinishedFading);
    }
        /// <summary> Fade to out to normal </summary>
    public void FadeFrom(float time)
    {
        FadeScript.fadeTime = time;
        FadeScript.FadeFromBlack();
    }

    private void OnLevelWasLoaded(int level)
    {
        Debug.Log("Running GameplayController's OnLevelWasLoaded for level "+level);
        if (/*PhotonNetwork.IsMasterClient &&*/ SceneManager.GetActiveScene().buildIndex==2)   //trigger the timer after a second of starting the new scene, to try and make sure everyone is on the same time
            GameplayNetworker.Instance.Invoke("Countdown", 1);

    }


}
