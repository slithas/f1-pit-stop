﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Simple script to trigger the local rotation of an object in the given axis
/// </summary>

public class TriggerRotation : MonoBehaviour {

    public Transform target;

    public float x;
    public float y;
    public float z;

    public bool active;


    public void TriggerRot(bool n)
    {
        active = n;
    }

    private void Update()
    {
        if (active)
        {
            target.localEulerAngles += new Vector3(x,y,z);
        }
    }
}
