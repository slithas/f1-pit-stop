﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Photon.Pun;
using BNG;

//TODO: check with 2 handed grabbables

namespace RealSim
{
    public class GrabbableNetworkProxy : MonoBehaviour
    {

        #region variables

        private PhotonView  ThePhotonView;
        private Grabbable   TheGrabbable;

        #endregion

        private void OnEnable()
        {
            CheckForAndAssignRequiredComponets();
         //   TheGrabbable.objectGrabbedEvent += AssignIsHeldCall;
         //   TheGrabbable.objectLetGoEvent += AssignIsHeldCall;
        }

        private void OnDisable()
        {
         //   TheGrabbable.objectGrabbedEvent -= AssignIsHeldCall;
         //   TheGrabbable.objectLetGoEvent -= AssignIsHeldCall;
        }

        /// <summary>
        /// Get components from gameobject this script is attached to
        /// </summary>
        private void CheckForAndAssignRequiredComponets()
        {
            ThePhotonView = GetComponent<PhotonView>();
            if(ThePhotonView == null)
            {
                this.enabled = false;
            }

            TheGrabbable = GetComponent<Grabbable>();
            if(TheGrabbable == null)
            {
                this.enabled = false;
            }
        }

        #region remote prodecure calls

        /// <summary>
        /// Send message to other users that grabble is being held and change owner
        /// </summary>
        /// <param name="isHeld"></param>
        public void AssignIsHeldCall(bool isHeld)
        {
            TheGrabbable.BeingHeld = isHeld;
            if (isHeld)
            {
                ThePhotonView.RequestOwnership();
                //TheGrabbable.rigid.isKinematic = false;
            }

            ThePhotonView.RPC("AssignIsHeld", RpcTarget.Others, isHeld);
        }

        /// <summary>
        /// Change the owner of the grabbable and mark as held
        /// </summary>
        /// <param name="isHeld"></param>
        [PunRPC]
        public void AssignIsHeld(bool isHeld)
        {
            TheGrabbable.BeingHeld = isHeld;
            //TheGrabbable.rigid.isKinematic = isHeld;   // turn kinematic on so we dont get object fighting with gravity.
        }

        #endregion

    }
}
    
