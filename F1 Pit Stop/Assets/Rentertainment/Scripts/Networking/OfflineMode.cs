using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
/// <summary>
/// setup offline mode of Photon, and how the menu shows up (triggered when not yet enabled, present auto when is).
/// </summary>


public class OfflineMode : MonoBehaviour
{
    public Text text;

    Canvas canvas;
    bool showCanvas;

    // Start is called before the first frame update
    void Start()
    {
        canvas = GetComponent<Canvas>();

        if (PhotonNetwork.OfflineMode)
            showCanvas = true;
        
    }

    // Update is called once per frame
    void Update()
    {
        canvas.enabled = showCanvas;

        if (BNG.InputBridge.Instance.XButtonDown)
            showCanvas = !showCanvas;

        if (PhotonNetwork.OfflineMode)
        {
            text.text = "Enabled";
            text.color = Color.yellow;
        }
        else
        {
            text.text = "Disabled";
            text.color = Color.grey;
        }

        if (Input.GetKeyDown(KeyCode.O)) OfflineSet();
    }

    public void OfflineSet()
    {
        PhotonNetwork.OfflineMode = !PhotonNetwork.OfflineMode;
    }

}
