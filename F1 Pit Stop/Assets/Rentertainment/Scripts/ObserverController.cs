using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObserverController : MonoBehaviour
{
    public GameObject VRPlayer;
    public static ObserverController Instance;
    public bool observer;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        if (UnityEngine.XR.InputDevices.GetDeviceAtXRNode(UnityEngine.XR.XRNode.Head).name == null)
        {
            Debug.Log("No VR Headset Attached.");
            VRPlayer.SetActive(false);
            GetComponent<OVRScreenFade>().enabled = true;
            observer = true;
        }
       else
        {
            Debug.Log("VR Headset Detected. " + UnityEngine.XR.InputDevices.GetDeviceAtXRNode(UnityEngine.XR.XRNode.Head).name);
            gameObject.SetActive(false);
            observer = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
