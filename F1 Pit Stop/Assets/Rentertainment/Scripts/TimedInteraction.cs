using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Used for detecting how long a trigger has been active for, interacted with a specific object, and responding with an event after the set amount of time
/// </summary>

public class TimedInteraction : MonoBehaviour
{
    public float tic;
    public float targetTime=1;
    public string targetName;

    public UnityEvent onStart;
    public UnityEvent onTime;

    bool started;
    bool ended;

    private void Update()
    {
        if (tic >= targetTime && !ended)
        {
            ended = true;
            onTime.Invoke();
        }
    }


    private void OnTriggerStay(Collider other)
    {
        if (other.name == targetName)
        {
            tic += Time.deltaTime;

            if (!started)
            {
                onStart.Invoke();
            }
            started = true;
        }
    }
}
