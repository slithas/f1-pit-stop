using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrillInteraction : MonoBehaviour
{
    BNG.SustainedHaptics haptics;
    TriggerRotation rotation;

    public AudioSource drilling;

    private void Start()
    {
        haptics = GetComponent<BNG.SustainedHaptics>();
        rotation = GetComponent<TriggerRotation>();
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "DrillZone")
        {
            DrillState(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "DrillZone")
        {
            DrillState(false);
        }
    }

    public void DrillState(bool n)
    {
        haptics.haptic = n;
        rotation.active = n;

        if (n && !drilling.isPlaying)
            drilling.Play();
        else
            drilling.Stop();
    }
}
