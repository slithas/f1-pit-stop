﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using BNG;
using System;

/// <summary>
/// This item can be interacted with and allows certain objects to snap to it.
/// </summary>
public class SnapPoint : MonoBehaviour
{
    public string[] snapNames;

    public Transform target;
    Grabbable targetGrab;
    MeshRenderer mesh;

    public bool attached;

    [Tooltip("If true, item won't be grabbable once placed")]
    public bool keep;

    [Tooltip("Objects listed here will disappear when Snap is active. For addional elements of the object, such as children.")]
    public GameObject[] holoObjs;

    public SnapEvent OnSnap;
    public SnapEvent OnUnsnap;

    SnapPointOptions spo;

    private void Start()
    {
        mesh = GetComponent<MeshRenderer>();
        spo = GetComponent<SnapPointOptions>();
    }

    // Update is called once per frame
    void Update()
    {
        if (targetGrab != null)
        {
            if (!targetGrab.BeingHeld && !attached)  //check that the object is present, able to be grabbed, and isn't currently being grabbed
            {
                target.GetComponent<Rigidbody>().isKinematic = true;
                target.position = transform.position;
                target.rotation = transform.rotation;
                mesh.enabled = false;
                attached = true;

                foreach (var item in holoObjs)
                    item.SetActive(false);

                if (keep)   //if ture, item cannot be picked up again, to reduce chance of detaching equipment unintentionally
                    targetGrab.enabled = false;

                OnSnap.Invoke(targetGrab);
                if (spo != null) 
                    spo.Snap();
            }
            if(targetGrab.BeingHeld && attached)
            {
                Detatch();
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (target == null)
            for (int i = 0; i < snapNames.Length; i++)
            {
                if (other.name == snapNames[i])  //set the target in prep for snap
                {
                    target = other.transform;
                    if (target.GetComponent<Grabbable>() != null)
                    {
                        targetGrab = target.GetComponent<Grabbable>();
                    }
                    return;
                }
            }
    }

    private void OnTriggerExit(Collider other)
    {
        if(targetGrab != null)
            if (other == targetGrab.gameObject)
                Detatch();
    }

    void Detatch()
    {
        target = null;
        targetGrab = null;
        mesh.enabled = true;
        attached = false;

        foreach (var item in holoObjs)
            item.SetActive(true);

        OnUnsnap.Invoke(targetGrab);
        if (spo != null)
            spo.Unsnap();
    }

    /// <summary>
    /// Unity Event to trigger when something attaches or detaches with  Grabbable
    /// </summary>
    [System.Serializable]
    public class SnapEvent : UnityEvent<Grabbable> { }
}
