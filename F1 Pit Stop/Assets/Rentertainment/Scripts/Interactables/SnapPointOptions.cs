using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// For use with SnapPoint, allows alternate UnityEvents to be triggered depending on which target object has snapped in
/// </summary>

public class SnapPointOptions : MonoBehaviour
{
    [System.Serializable]
    public class Actions
    {
        public string name;
        public UnityEvent onSnap;    //these should match up with the number of options for Snap.snapNames.
        public UnityEvent onUnsnap;
    }

    public List<Actions> actions = new List<Actions>();

    SnapPoint snap;

    // Start is called before the first frame update
    void Start()
    {
        snap = GetComponent<SnapPoint>();
    }

    public void Snap()  //Invoked by the SnapPoint script; checks each item to see if it's name matches the name of the target. These should be found in Snap.SnapNames too.
    {
        foreach (var Actions in actions)
        {
            if (Actions.name == snap.target.name)
                Actions.onSnap.Invoke();
        }
    }
    public void Unsnap() //As above but for Unsnap
    {
        foreach (var Actions in actions)
        {
            if(Actions != null && snap != null)
            if(snap.target != null)
                if (Actions.name == snap.target.name)
                    Actions.onUnsnap.Invoke();
        }
    }
}
