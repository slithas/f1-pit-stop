﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using System.Linq;


[RequireComponent(typeof(OVRGrabbable))]
public class OculusGrabInteraction : MonoBehaviour {

    OVRGrabbable grabby;

    public UnityEvent OnBeginInteraction;
    public UnityEvent OnEndInteraction;

    bool grabbed;

    // Use this for initialization
    void Start () {
        grabby = GetComponent<OVRGrabbable>();
	}
	
	// Update is called once per frame
	void Update () {

        //begin intereaction
        if (grabby.isGrabbed && !grabbed){
            OnBeginInteraction.Invoke();
            grabbed = true;
        }
        //end interaction
        if (!grabby.isGrabbed && grabbed)
        {
            OnEndInteraction.Invoke();
            grabbed = false;
        }

    }
}
