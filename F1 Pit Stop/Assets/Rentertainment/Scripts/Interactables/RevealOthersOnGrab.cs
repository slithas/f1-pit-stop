﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BNG;


namespace RealSim
{ 
    /// <summary>
    /// when this object is grabbed, other objects appear. This is used to reveal where the input points are for the interactable this is attached to.
    /// </summary>

    public class RevealOthersOnGrab : GrabbableEvents
    {
        public GameObject[] gameObjects; 
        public bool hideOnDrop; //object holos should turn off when the item is let go

        protected override void Awake()
        {
            base.Awake();

            foreach (GameObject item in gameObjects)
            {
                if(item != null)
                    item.SetActive(false);
              //  else
                 //   RealSimController.Instance.Logger.AddToLog("Missing reference, item is null", Logging.MessageType.Error);
            }
        }

         void Start()
        {
            GetComponent<Rigidbody>().isKinematic = true;
        }

        public override void OnGrab(Grabber grabber)
        {
            foreach (GameObject item in gameObjects)        
                item.SetActive(true);
        
            base.OnGrab(grabber);
        }
    
        public override void OnRelease()
        {
            if (hideOnDrop)
            {
                Invoke("HideObjects", 0.5f);
            }
            base.OnRelease();
        }

        public void HideObjects()
        {
            foreach (GameObject item in gameObjects)
            {
                item.SetActive(false);
            }
        }
        private void OnDestroy() //mainly used for medicine that is destroyed when thrown in the bin
        {
            HideObjects();
        }
    }
}