﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Mimic another gameobject's position and rotation
/// </summary>

public class FollowObjectAndRot : MonoBehaviour
{
    public Transform target;
     

    // Update is called once per frame
    void Update()
    {
        transform.position = target.position;
        transform.rotation = target.rotation;
    }
}
