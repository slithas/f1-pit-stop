﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using BNG;

namespace RealSim
{
    public class EventOnGrab : GrabbableEvents
    {
        public UnityEvent onClick;
        // Start is called before the first frame update
        public override void OnGrab(Grabber grabber)
        {
            onClick.Invoke();
        }
    }
}