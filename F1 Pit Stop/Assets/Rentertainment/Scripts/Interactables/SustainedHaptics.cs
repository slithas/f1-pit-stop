using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BNG
{/// <summary>
/// using BNG to bridge, allows for sustained haptics
/// </summary>
    public class SustainedHaptics : GrabbableEvents
    {
        public float VibrateFrequency = 0.3f;
        public float VibrateAmplitude = 0.1f;
        public float VibrateDuration = 1f;

        Grabber currentGrabber;
        public bool haptic;
        public override void OnGrab(Grabber grabber)
        {
            // Store grabber so we can use it if we need to vibrate the controller
            currentGrabber = grabber;                    
        }
        // Update is called once per frame
        void Update()
        {
            if (haptic)
                doHaptics(currentGrabber.HandSide);
        }

        void doHaptics(BNG.ControllerHand touchingHand)
        {
            if (input)
            {
                input.VibrateController(VibrateFrequency, VibrateAmplitude, VibrateDuration, touchingHand);
            }
        }

        public void SetHaptics(bool n)
        {
            haptic = n;
        }
    }
}
