using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HaltCar : MonoBehaviour
{
    public int round=3;
    public bool invert;
    // Start is called before the first frame update
    void Start()
    {
        if (GameplayController.Instance.runNo == round && !invert || GameplayController.Instance.runNo != round && invert)
            gameObject.SetActive(false);
    }

}
