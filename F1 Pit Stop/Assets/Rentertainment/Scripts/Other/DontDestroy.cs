using UnityEngine;

public class DontDestroy : MonoBehaviour
{
    public static DontDestroy Instance;
    public GameObject[] safeObjects;
    void Awake()
    {
            foreach (var item in safeObjects)
        {
            if (Instance != null && Instance != this)
                Destroy(item);
            else
                DontDestroyOnLoad(item);
            }
        DontDestroyOnLoad(this);
        Instance = this;
    }
}
