using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraChange : MonoBehaviour
{
    public Transform[] locations;
    int cam;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            cam++;
            if (cam>=locations.Length) cam = 0;
            transform.position = locations[cam].position;
            transform.rotation = locations[cam].rotation;
        }
    }
}
