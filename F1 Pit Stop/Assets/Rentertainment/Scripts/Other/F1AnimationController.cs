using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class F1AnimationController : MonoBehaviour
{
    public Animator animator;
    public static F1AnimationController Instance;
    GameplayController gc;

    // Start is called before the first frame update
    void Awake()
    {
        Instance = this;
        animator = GetComponent<Animator>();
    }

    private void Start()
    {
        gc = GameplayController.Instance;
    }

    public void StartAnim()
    {
        if(gc.runNo!=0)
        animator.SetTrigger("Done");
        animator.SetInteger("Round", gc.runNo);
    }
}
