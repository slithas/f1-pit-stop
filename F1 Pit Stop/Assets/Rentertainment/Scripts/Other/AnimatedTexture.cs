﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatedTexture : MonoBehaviour {

    public Texture[] textures1;
    public Texture[] textures2;
    public Texture[] textures3;
    public Texture[] textures4;
    public Texture[] textures5;
    public float changeInterval = 0.33F;
    public Renderer rend;

    public int[] order;

    private void Start()
    {
        rend = GetComponent<Renderer>();
    }
    // Change renderer's texture each changeInterval/
    // seconds from the texture array defined in the inspector.

    void Update()
    {
        if (textures1.Length == 0)
            return;

        int index = Mathf.FloorToInt(Time.time / changeInterval);
        index = index % textures1.Length;
        
            rend.materials[order[0]].mainTexture = textures1[index];
        rend.materials[order[1]].mainTexture = textures2[index];

    }
}
