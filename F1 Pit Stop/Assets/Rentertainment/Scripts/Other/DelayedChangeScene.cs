using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class DelayedChangeScene : MonoBehaviourPun
{
    public float tic=5;
    public int level;
    public bool auto;

    public static DelayedChangeScene Instance;

    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        if (GameplayController.Instance.runNo == 3) level = 0;  //here to allow a return to the Lobby

        // ensure that all players play on the same map
        PhotonNetwork.AutomaticallySyncScene = true;
        if (auto)
            DelayedChange();
    }

    public void DelayedChange()
    {
        StartCoroutine(Tic());
    }

    IEnumerator Tic()
    {
        yield return new WaitForSeconds(tic);
       // if(PhotonNetwork.IsMasterClient)
        PhotonNetwork.LoadLevel(level);        
    }
}
