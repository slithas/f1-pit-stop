using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerBaseController : MonoBehaviour
{

    public static PlayerBaseController Instance;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
    }

    private void OnLevelWasLoaded(int level)
    {

        if (SceneManager.GetActiveScene().buildIndex == 0)
        {
            GameplayController.Instance.ResetSequence();
        }
    }
}
