﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelmColour : MonoBehaviour {

    public GameObject helm;
    public GameObject handL;
    public GameObject handR;
    public Material[] matHand;
    public Material[] matHelm;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void PlayerColour(int p)
    {
        gameObject.transform.localPosition = Vector3.zero;

        Debug.Log("Colour change " + p);
        helm.GetComponent<MeshRenderer>().material = matHelm[p];
        handL.GetComponent<MeshRenderer>().material = matHand[p];
        handR.GetComponent<MeshRenderer>().material = matHand[p];
    }
}
