﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Used for making the object this is attached to respawn at it's starting location when it falls to the floor
/// </summary>

public class Respawn : MonoBehaviour
{
    Vector3 startPos;
    Vector3 startRot;


    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.localPosition;
        startRot = transform.localEulerAngles;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "ReSpawn")
        {
            GetComponent<Rigidbody>().isKinematic = true;
            transform.localPosition = startPos;
            transform.localEulerAngles = startRot;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "ReSpawn")
        {
            GetComponent<Rigidbody>().isKinematic = true;
            transform.localPosition = startPos;
            transform.localEulerAngles = startRot;
        }
    }
}
