using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Countdown : MonoBehaviour
{
    public Text text;
    public GameObject scores;
    bool started;

    // Start is called before the first frame update
    void Start()
    {
        text.text = "";
    }

    // Update is called once per frame
    void Update()
    {
        if(GameplayController.Instance.countdown && !started)
        {
            started = true;
            StartCoroutine(tic());
        }
    }

    IEnumerator tic()
    {
        GetComponent<AudioSource>().Play();
        text.text = "3";
        yield return new WaitForSeconds(1);
        text.text = "2";
        yield return new WaitForSeconds(1);
        text.text = "1";
        yield return new WaitForSeconds(1);
        text.text = "Go!";
        yield return new WaitForSeconds(1);
        scores.SetActive(true);
        text.text = "";
    }
}
