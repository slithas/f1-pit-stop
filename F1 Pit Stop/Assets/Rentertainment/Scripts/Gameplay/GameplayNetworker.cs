using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// The Networked element for the Gameplay Controller to reference. This is in beacuse Don't Destroy doesn't work with PhotonView.
/// </summary>

public class GameplayNetworker : MonoBehaviourPun
{
    public static GameplayNetworker Instance;
    public GameplayController gc;

    private void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        gc = GameplayController.Instance;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) PhotonNetwork.ReconnectAndRejoin();
    }

    public void Countdown()
    {
        this.photonView.RPC("RPCCountdown", RpcTarget.AllBufferedViaServer);
    }

    [PunRPC]
    void RPCCountdown()
    {
        GameplayController.Instance.countdown = true;
    }

    public void SetScore(int player)
    {
        Debug.Log("Setting score for player " + player);
        this.photonView.RPC("RPCSetScore", RpcTarget.AllBuffered, player, gc.timer);
    }

    // Sets the score, sending the data on the player and the time shown on their device. Tic input should be 'timer'.
    [PunRPC]
    void RPCSetScore(int player, float tic)
    {
        Debug.Log("Score of: " + tic + " for player " + player);
        gc.time[player] = tic;
    }
    public void SetPlayerNo(int player)
    {
        this.photonView.RPC("RPCSetPlayerNo", RpcTarget.AllBuffered,player);
    }

    [PunRPC]
    void RPCSetPlayerNo(int player)
    {
        gc.playerNo = player;
    }

    public void TeleportPlayer(Vector3 newPosition, bool fadeOut = true)
    {
        this.photonView.RPC("RPCTeleportPlayer", RpcTarget.AllBuffered, newPosition, fadeOut);
    }

    [PunRPC]
    void RPCTeleportPlayer(Vector3 newPosition, bool fadeOut)
    {
        gc.TeleportPlayer(newPosition, fadeOut);
    }

    public void StartSequence()
    {
        if (PhotonNetwork.IsConnected) //don't let this run if not connected to a server, it won't work
        {
            gc.StartSequence();
            DelayedChangeScene.Instance.DelayedChange();
        }
    }
}
