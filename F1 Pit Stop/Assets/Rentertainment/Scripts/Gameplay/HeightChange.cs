using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BNG
{
    public class HeightChange : MonoBehaviour
    {
        void Update()
        {
            if (InputBridge.Instance.BButton)
            {
                transform.position += new Vector3(0, 0.01f, 0);
            }
            if (InputBridge.Instance.AButton)
            {
                transform.position += new Vector3(0, -0.01f, 0);
            }
        }
    }
}
