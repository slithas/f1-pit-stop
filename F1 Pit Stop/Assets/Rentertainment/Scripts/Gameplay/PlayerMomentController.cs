using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BNG
{
    public class PlayerMomentController : MonoBehaviour
    {
        bool set = true;
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (InputBridge.Instance.YButtonDown)
            {
                if (set) set = false;
                else set = true;

                GetComponent<SmoothLocomotion>().enabled = set;
                GetComponent<PlayerRotation>().enabled = set;
            }
        }
    }
}
